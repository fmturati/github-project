import React, { Component, Fragment } from "react";
import axios from "axios";
import "./App.css";
import SearchScreen from "./components/searchScreen/searchScreen";
import IssuesScreen from "./components/issuesScreen/issuesScreen";

const accessToken = process.env.REACT_APP_TOKEN;
const baseURL = process.env.REACT_APP_BASE_URL;
const config = {
  headers: { Authorization: "Bearer" + accessToken }
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterActive: "issues?state=all",
      hasError: null,
      issues: [],
      isSearch: false,
      isLoading: false,
      repoSearched: null
    };
  }

  getRepo = e => {
    e.preventDefault();
    const repoSearched = e.target.elements.searchRepo.value;
    const filterActive = this.state.filterActive;
    const setUserRepo = repoSearched.replace("https://github.com/", "");

    // TO ADD PAGINATION AND PER PAGE PARAMS:
    // ?page=2 & per_page=100`

    this.getAPI(baseURL, setUserRepo, filterActive, repoSearched);
    e.target.elements.searchRepo.value = "";
  };

  setFilter = filterElement => {
    this.setState({ filterActive: filterElement, isLoading: true });
    const repoName = this.state.repoSearched;
    const setUserRepo = repoName.replace("https://github.com/", "");
    let url = `${baseURL}/repos/${setUserRepo}/${filterElement}`;

    axios
      .get(url, config)
      .then(res => {
        const repos = res.data;
        this.setState({
          issues: repos,
          filterActive: filterElement,
          isLoading: false,
          hasError: null
        });
      })
      .catch(err => console.log("Something went wrong...", err));
  };

  handlerCloseScreen = () => {
    this.setState({
      isSearch: !this.state.isSearch,
      repoSearched: null,
      filterActive: "issues?state=all"
    });
  };

  getAPI(baseURL, setUserRepo, filterActive, repoSearched) {
    filterActive = this.state.filterActive;

    let url = `${baseURL}/repos/${setUserRepo}/${filterActive}`;
    axios
      .get(url, config)
      .then(res => {
        const repos = res.data;
        this.setState({
          issues: repos,
          isSearch: true,
          repoSearched: repoSearched,
          isLoading: false,
          filterActive: filterActive,
          hasError: null
        });
      })
      .catch(err => {
        this.setState({ hasError: err });
        console.log("Something went wrong...", err);
      });
  }

  render() {
    const { isSearch, hasError, isLoading, filterActive } = this.state;

    const transitionScreen = {
      transform: isSearch ? "translateY(-2000px)" : "translateY(0)",
      transition: "all 0.6s cubic-bezier(0.94, 0.01, 0.58, 1)",
      position: "absolute",
      width: "100%",
      height: "100%"
    };
    const transitionsResult = {
      transform: isSearch ? "translateX(0px)" : "translateX(-3000px)",
      transition: "all 0.8s ease-in-out",
      width: "100%",
      height: "100%",
      position: "absolute"
    };

    return (
      <Fragment>
        <div style={transitionScreen}>
          <SearchScreen getRepo={this.getRepo} hasError={hasError} />
        </div>
        <div style={transitionsResult}>
          <IssuesScreen
            data={this.state}
            isLoading={isLoading}
            handlerCloseScreen={this.handlerCloseScreen}
            setFilter={this.setFilter}
            filterActive={filterActive}
          />
        </div>
      </Fragment>
    );
  }
}

export default App;
