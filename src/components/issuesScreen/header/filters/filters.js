import React from "react";
import "./filters.css";

const Filters = props => {
  const { filterActive } = props.data;

  const filters = [
    {
      name: "All Issues",
      value: "issues?state=all"
    },
    {
      name: "Open Issues",
      value: "issues?state=open"
    },
    {
      name: "Closed Issues",
      value: "issues?state=closed"
    },
    {
      name: "Pull Requests",
      value: "pulls?pull_request"
    }
  ];

  const createFiltersButtons = filters.map((filter, index) => {
    const { name, value } = filter;
    return (
      <li
        className={`filter-item ${filterActive === value &&
          "filter-item--active"}`}
        key={index}
        value={value}
        name="filter"
        onClick={() => {
          props.setFilter(value);
        }}
      >
        {name}
      </li>
    );
  });

  return (
    <div className="filter-section">
      <ul className="filter-list">{createFiltersButtons}</ul>
    </div>
  );
};

export default Filters;
