import React, { Fragment } from "react";
import closeIcon from "../../../static/img/close.svg";
import "./header.css";
import Filters from "./filters/filters";

const Header = props => {
  const { data, handlerCloseScreen, repoName, setFilter } = props;
  return (
    <Fragment>
      <div className="header">
        <h1 className="header-title">GitHub Issue Viewer</h1>
        <p className="header-repo-title">{repoName}</p>
      </div>
      <div className="header-navbar">
        <Filters data={data} setFilter={setFilter} />
        <a
          onClick={handlerCloseScreen}
          className="header-close-icon zoom-element"
        >
          <img src={closeIcon} alt="Close Screen Icon" />
        </a>
      </div>
    </Fragment>
  );
};

export default Header;
