import React from "react";
import "./issuesScreen.css";
import Header from "./header/header";
import Issue from "./issue/issue";
import Loader from "../loader/loader";

const IssuesScreen = props => {
  const {
    data,
    filterActive,
    isLoading,
    handlerCloseScreen,
    setFilter
  } = props;

  const showAllIssues = data.issues.map((issue, index) => {
    return (
      <Issue
        filterActive={filterActive}
        issue={issue}
        index={index}
        key={index}
      />
    );
  });

  return (
    <div>
      <Header
        data={data}
        filterActive={filterActive}
        handlerCloseScreen={handlerCloseScreen}
        isLoading={isLoading}
        repoName={data.repoSearched}
        setFilter={setFilter}
      />

      <div className="container">
        {isLoading ? (
          <Loader />
        ) : data.issues.length !== 0 ? (
          // if the repo has issues...
          <div className="issues-grid">{showAllIssues}</div>
        ) : (
          // else ...
          <div className="no-issues-message">
            <h3>No Issues to list! Please, try to search for another Repo!</h3>
          </div>
        )}
      </div>
    </div>
  );
};

export default IssuesScreen;
